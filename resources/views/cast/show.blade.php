@extends('adminlte.master')
@section('content')
<div class="container my-5">
<div class="card text-white bg-dark mb-3" style="max-width: 25rem;">
    <div class="card-header">Show Cast {{$cast->id}}</div>
    <div class="card-body">
        <h4> Nama Cast : {{$cast->nama}}</h4>
        <h4> Umur :{{$cast->umur}}</h4>
        <h4> Bio :{{$cast->bio}}</h4>
    </div>
</div>
@endsection